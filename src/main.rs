extern crate chrono;

extern crate logger;
#[macro_use]
extern crate log;
extern crate getopts;

extern crate toml;
#[macro_use]
extern crate serde_derive;

use getopts::{Matches, Options};

use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::path::PathBuf;
use std::{thread, time};

use logger::{ColorConsole, Console, ConsoleColor, LevelFilter};

use chrono::prelude::Local;
// ----------------------------------------------------------------------------
const SLEEP_DURATION: u64 = 500;
// ----------------------------------------------------------------------------
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const NAME: &str = "logfilter";

enum OpMode {
    Watch,
    FilterOnce,
}

struct CliArgs {
    mode: OpMode,
    silent: bool,
    logfile: PathBuf,
    configfile: PathBuf,
    outputdir: PathBuf,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Deserialize)]
struct Config {
    highlight: Option<Vec<HighlightSetting>>,
    redirect: Option<Vec<RedirectSetting>>,
}
// ----------------------------------------------------------------------------
struct Settings {
    highlight: Vec<HighlightSetting>,
    redirect: Vec<RedirectSetting>,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Deserialize)]
enum HighlightColor {
    Blue,
    Green,
    Red,
    Yellow,
    Magenta,
    Cyan,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Deserialize)]
struct HighlightSetting {
    keyword: String,
    skip: Option<bool>,
    color: HighlightColor,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Deserialize)]
struct RedirectSetting {
    keyword: String,
    file: String,
    info: Option<bool>,
    replace: Option<ReplaceSetting>,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Deserialize)]
struct ReplaceSetting {
    key: String,
    with: String,
}
// ----------------------------------------------------------------------------
fn setup_option() -> Options {
    let mut opts = Options::new();

    // misc
    opts.optflag("h", "help", "print this help menu");

    opts.optopt(
        "l",
        "log-file",
        "path to logfile to watch or filter.",
        "FILENAME",
    );
    opts.optopt(
        "c",
        "conf-file",
        "config file with settings. default: logfilter-config.toml",
        "FILENAME",
    );

    opts.optopt(
        "o",
        "output-dir",
        "defines root output-directory for generated file(s).",
        "DIRECTORY",
    );
    opts.optflag(
        "",
        "dont-watch",
        "process logfile and immediately exit afterwards.",
    );
    opts.optflag("", "silent", "no information output aside from highlights.");

    // misc
    opts.optflag("v", "verbose", "show debug messages");
    opts.optflag("", "very-verbose", "show more debug messages");

    opts
}
// ----------------------------------------------------------------------------
fn check_dir(dir: &str, name: &str) -> Result<PathBuf, String> {
    // check if outdir exists
    let dir = PathBuf::from(dir);
    if !dir.exists() || !dir.is_dir() {
        Err(format!("{} [{}] does not exist", name, dir.display()))
    } else {
        Ok(dir)
    }
}
// ----------------------------------------------------------------------------
fn check_file(file: &str, errname: &str) -> Result<PathBuf, String> {
    let file = PathBuf::from(file);
    if !file.exists() || !file.is_file() {
        Err(format!("{} [{}] does not exist", errname, file.display()))
    } else {
        Ok(file)
    }
}
// ----------------------------------------------------------------------------
fn parse_arguments(found: &Matches) -> Result<CliArgs, String> {
    let loglevel = if found.opt_present("very-verbose") {
        LevelFilter::Trace
    } else if found.opt_present("v") {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };
    let _ = logger::init(loglevel);

    // extract options
    let param_logfile = found
        .opt_str("l")
        .ok_or_else(|| String::from("missing log-file parameter"))?;
    let param_conffile = found
        .opt_str("c")
        .unwrap_or_else(|| String::from("logfilter-config.toml"));

    // dirs
    let param_output_dir = found.opt_str("o");

    let (mode, logfile) = if found.opt_present("dont-watch") {
        // logfile may not be present at this moment
        // verify only if dont-watch
        (OpMode::FilterOnce, check_file(&param_logfile, "log-file")?)
    } else {
        (OpMode::Watch, PathBuf::from(param_logfile))
    };

    let configfile = check_file(&param_conffile, "config file")?;

    // check for optional output directory parameters
    let outputdir = match param_output_dir {
        Some(dir) => check_dir(&dir, "output directory")?,
        None => {
            let mut logfile_dir = logfile.clone();
            logfile_dir.pop();
            if logfile_dir.to_string_lossy() == "" {
                PathBuf::from(".")
            } else {
                check_dir(&logfile_dir.to_string_lossy(), "output directory")?
            }
        }
    };

    Ok(CliArgs {
        mode,
        silent: found.opt_present("silent"),
        logfile,
        configfile,
        outputdir,
    })
}
// ----------------------------------------------------------------------------
fn print_usage(program: &str, opts: &Options) {
    let brief = format!(
        "{} v{} \nUsage: {} [options]",
        NAME,
        VERSION.unwrap_or("unknown"),
        program
    );
    print!("{}", opts.usage(&brief));
}
// ----------------------------------------------------------------------------
impl HighlightSetting {
    // ------------------------------------------------------------------------
    fn normalize(&mut self) {
        self.keyword = self.keyword.to_lowercase();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl RedirectSetting {
    // ------------------------------------------------------------------------
    fn normalize(&mut self) {
        self.keyword = self.keyword.to_lowercase();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<&HighlightColor> for ConsoleColor {
    fn from(col: &HighlightColor) -> ConsoleColor {
        match col {
            HighlightColor::Blue => ConsoleColor::Blue,
            HighlightColor::Green => ConsoleColor::Green,
            HighlightColor::Red => ConsoleColor::Red,
            HighlightColor::Yellow => ConsoleColor::Yellow,
            HighlightColor::Magenta => ConsoleColor::Magenta,
            HighlightColor::Cyan => ConsoleColor::Cyan,
        }
    }
}
// ----------------------------------------------------------------------------
fn read_config(configfile: &str) -> Result<Settings, String> {
    trace!("> reading configfile {}", configfile);
    let mut conf = File::open(configfile).map_err(|e| e.description().to_string())?;
    let mut buffer = String::new();

    conf.read_to_string(&mut buffer)
        .map_err(|e| e.description().to_string())?;

    let mut conf: Config = toml::from_str(&buffer).map_err(|e| e.description().to_string())?;

    let mut settings = Settings {
        highlight: conf.highlight.take().unwrap_or_default(),
        redirect: conf.redirect.take().unwrap_or_default(),
    };

    settings.highlight.iter_mut().for_each(|h| h.normalize());
    settings.redirect.iter_mut().for_each(|f| f.normalize());

    Ok(settings)
}
// ----------------------------------------------------------------------------
fn init_destination_files<'a>(
    settings: &'a Settings,
    outputdir: &PathBuf,
) -> Result<HashMap<&'a str, BufWriter<std::fs::File>>, String> {
    // create all files
    let creation_time = Local::now().format("%Y-%m-%d %H:%M:%S");

    let mut files = HashMap::with_capacity(settings.redirect.len());
    let mut destination = outputdir.join("dummy.log");

    for filter in &settings.redirect {
        // multiple redirects may use the same file
        if !files.contains_key(filter.file.as_str()) {
            destination.set_file_name(&filter.file);

            let mut file = BufWriter::new(
                OpenOptions::new()
                    .create(true)
                    .append(true)
                    .open(&destination)
                    .map_err(|e| e.description().to_string())?,
            );

            file.write(
                format!(
                    "#--- {} -------------------------------------------------------\n",
                    creation_time
                )
                .as_bytes(),
            )
            .map_err(|e| e.description().to_string())?;

            files.insert(filter.file.as_str(), file);
        }
    }
    Ok(files)
}
// ----------------------------------------------------------------------------
fn highlight(lowercased: &str, line: &str, highlights: &[HighlightSetting]) {
    for highlight in highlights {
        if lowercased.contains(&highlight.keyword) {
            if !highlight.skip.unwrap_or_default() {
                Console::set_color((&highlight.color).into());
                println!("{}", line);
                Console::set_color(ConsoleColor::Default);
            }
            break;
        }
    }
}
// ----------------------------------------------------------------------------
fn redirect(
    lowercased: &str,
    line: &str,
    redirects: &[RedirectSetting],
    destination: &mut HashMap<&str, BufWriter<std::fs::File>>,
) -> Result<(), String> {
    for redirect in redirects {
        let catch_all = redirect.keyword == "*";
        if catch_all || lowercased.contains(&redirect.keyword) {
            if let Some(ref mut destination) = destination.get_mut(redirect.file.as_str()) {
                match &redirect.replace {
                    Some(replace) => {
                        destination.write(line.replace(&replace.key, &replace.with).as_bytes())
                    }
                    None => destination.write(line.as_bytes()),
                }
                .map_err(|e| e.description().to_string())?;

                destination
                    .write(b"\n")
                    .map_err(|e| e.description().to_string())?;
            }
            if !catch_all {
                break;
            }
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn watch_log(
    silent: bool,
    logfile: &PathBuf,
    configfile: &PathBuf,
    outputdir: &PathBuf,
) -> Result<(), String> {
    if !silent {
        info!("WATCHING LOG: {}", logfile.display());
    }
    let settings = read_config(&configfile.to_string_lossy())?;

    let mut files = init_destination_files(&settings, outputdir)?;

    let logfile = logfile
        .as_path()
        .canonicalize()
        .map_err(|e| e.description().to_string())?;

    let mut file: Option<File> = None;

    let sleepduration = time::Duration::from_millis(SLEEP_DURATION);
    let mut last_filesize = 0;
    let mut buf = String::new();

    loop {
        if logfile.exists() && logfile.is_file() {
            if let Some(ref mut file) = file {
                let size = file
                    .metadata()
                    .map_err(|e| e.description().to_string())?
                    .len();

                if size != last_filesize {
                    buf.clear();

                    if size < last_filesize {
                        // file was recreated and new data was written -> read from start
                        file.seek(SeekFrom::Start(0))
                            .map_err(|e| e.description().to_string())?;
                        file.read_to_string(&mut buf)
                            .map_err(|e| e.description().to_string())?;

                        // print separator for highlights
                        println!(
                            "#--- {} -------------------------------------------------------",
                            Local::now().format("%Y-%m-%d %H:%M:%S")
                        );
                        files = init_destination_files(&settings, outputdir)?;
                    } else {
                        file.read_to_string(&mut buf)
                            .map_err(|e| e.description().to_string())?;
                    }

                    for line in buf.lines() {
                        let lc = line.to_lowercase();

                        highlight(&lc, &line, &settings.highlight);
                        redirect(&lc, &line, &settings.redirect, &mut files)?;
                    }
                    last_filesize = size;
                    files
                        .values_mut()
                        .try_for_each(|f| f.flush())
                        .map_err(|e| e.description().to_string())?;
                }

                thread::sleep(sleepduration);
            } else {
                file = Some(File::open(&logfile).map_err(|e| e.description().to_string())?);
                // print separator for highlights
                println!(
                    "#--- {} -------------------------------------------------------",
                    Local::now().format("%Y-%m-%d %H:%M:%S")
                );
                files = init_destination_files(&settings, outputdir)?;
            }
        } else {
            file = None;
            last_filesize = 0;
            thread::sleep(sleepduration);
        }
    }
}
// ----------------------------------------------------------------------------
fn filter_once(
    silent: bool,
    logfile: &PathBuf,
    configfile: &PathBuf,
    outputdir: &PathBuf,
) -> Result<(), String> {
    if !silent {
        info!("FILTERING LOG: {}", logfile.display());
    }
    let settings = read_config(&configfile.to_string_lossy())?;

    let input = File::open(logfile).map_err(|e| e.description().to_string())?;
    let buffer = BufReader::new(input);

    let mut files = init_destination_files(&settings, &outputdir)?;

    for line in buffer.lines() {
        let line = line.map_err(|e| e.description().to_string())?;
        let lc = line.to_lowercase();

        highlight(&lc, &line, &settings.highlight);
        redirect(&lc, &line, &settings.redirect, &mut files)?;
    }
    if !silent {
        info!("finished filtering.");
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn start_main() -> Result<(), i32> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let opts = setup_option();

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            logger::pre_init_fatal(f.to_string());
            print_usage(&program, &opts);
            return Err(1);
        }
    };

    // no free args
    if matches.opt_present("h") || !matches.free.is_empty() {
        print_usage(&program, &opts);
        return Ok(());
    }

    match parse_arguments(&matches) {
        Ok(args) => {
            if !args.silent {
                println!("{} v{}\n", NAME, VERSION.unwrap_or("unknown"));
            }
            match args.mode {
                OpMode::Watch => watch_log(
                    args.silent,
                    &args.logfile,
                    &args.configfile,
                    &args.outputdir,
                ),
                OpMode::FilterOnce => filter_once(
                    args.silent,
                    &args.logfile,
                    &args.configfile,
                    &args.outputdir,
                ),
            }
            .or_else(|errmsg: String| {
                error!("{}", errmsg);
                Err(1)
            })
        }
        Err(msg) => {
            error!("{}", msg);
            print_usage(&program, &opts);
            Err(1)
        }
    }
}
// ----------------------------------------------------------------------------
use std::process;

fn main() {
    let resultcode = match start_main() {
        Ok(_) => 0,
        Err(err) => err,
    };

    process::exit(resultcode);
}
// ----------------------------------------------------------------------------
